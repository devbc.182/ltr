# Genesis Block
configtxgen -outputBlock ./orderer/ltrgenesis.block -channelID ordererchannel -profile LandTitleRegistryOrdererGenesis

# Channel Tx
configtxgen -outputCreateChannelTx ./ltrchannel/ltrchannel.tx -channelID registrychannel -profile LandTitleRegistryChannel

# Bin Path
PATH="/workspaces/LTRegistry/bin:$PATH"
export PATH=$PATH:/usr/local/go/bin

# Channel creation
peer channel create -c registrychannel -f ./config/ltrchannel/ltrchannel.tx --outputBlock ./config/ltrchannel/ltrchannel.block -o $ORDERER_ADDRESS

# Channel Joining 
peer channel join -b ./config/ltrchannel/ltrchannel.block -o $ORDERER_ADDRESS

# Generate Crypto config
cryptogen generate --config=./crypto-config.yaml --output=crypto-config

# Network set up
docker network create --driver=bridge --subnet=192.168.2.0/24 --gateway=192.168.2.10 ltregistry

#
peer lifecycle chaincode package $CC_PACKAGE_FILE -p $CC_PATH --label $CC_LABEL
peer lifecycle chaincode install $CC_PACKAGE_FILE

landmgt.1.0-1.0:ae7ec373a628d210a4b0b8ddd2b2fb633f2ba5bf45d19313b3de7ef785179927
landmgt.1.0-1.0:ae7ec373a628d210a4b0b8ddd2b2fb633f2ba5bf45d19313b3de7ef785179927

peer lifecycle chaincode approveformyorg -n landmgt -v 1.0 -C registrychannel --sequence 1 --package-id $CC_PACKAGE_ID

peer lifecycle chaincode checkcommitreadiness -n landmgt -v 1.0 -C registrychannel --sequence 1

peer lifecycle chaincode commit -n landmgt -v 1.0 -C registrychannel --sequence 1

peer chaincode invoke -C registrychannel -n landmgt -c '{"function":"CreateLandTitle","Args":["Sample Title", "This is a description of the land."]}'

peer chaincode query -C registrychannel -n landmgt -c '{"function":"ReadLandTitle","Args":["1"]}'

peer chaincode invoke -C registrychannel -n landmgt -c '{"function":"DeleteLandTitle","Args":["1"]}'

peer lifecycle chaincode checkcommitreadiness -n landmgt -v 1.0 -C registrychannel --sequence 1

peer lifecycle chaincode querycommitted -n landmgt -C registrychannel

peer lifecycle chaincode package $CC_PACKAGE_FILE -p $CC_PATH --label $CC_LABEL
