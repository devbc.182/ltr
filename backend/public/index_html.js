document.addEventListener('DOMContentLoaded', () => {
    const createLandForm = document.getElementById('createLandForm');
    const updateLandForm = document.getElementById('updateLandForm');
    const transferLandForm = document.getElementById('transferLandForm');
    const readLandForm = document.getElementById('readLandForm');
    const resultDiv = document.getElementById('result');


    createLandForm.addEventListener('submit', async (event) => {
        event.preventDefault();
        const title = document.getElementById('title').value;
        const description = document.getElementById('description').value;

        try {
            const response = await fetch('/lands', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ title, description })
            });

            if (response.status === 204) {
                resultDiv.innerHTML = `<div class="alert alert-success">Land title created successfully</div>`;
            } else {
                const error = await response.text();
                resultDiv.innerHTML = `<div class="alert alert-danger">Error: ${error}</div>`;
            }
        } catch (error) {
            resultDiv.innerHTML = `<div class="alert alert-danger">Error: ${error.message}</div>`;
        }
    });

    updateLandForm.addEventListener('submit', async (event) => {
        event.preventDefault();
        const id = document.getElementById('updateId').value;
        const title = document.getElementById('updateTitle').value;
        const description = document.getElementById('updateDescription').value;

        try {
            const response = await fetch(`/lands/${id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ title, description })
            });

            if (response.status === 204) {
                resultDiv.innerHTML = `<div class="alert alert-success">Land title updated successfully</div>`;
            } else {
                const error = await response.text();
                resultDiv.innerHTML = `<div class="alert alert-danger">Error: ${error}</div>`;
            }
        } catch (error) {
            resultDiv.innerHTML = `<div class="alert alert-danger">Error: ${error.message}</div>`;
        }
    });

    transferLandForm.addEventListener('submit', async (event) => {
        event.preventDefault();
        const id = document.getElementById('transferId').value;
        const newOwnerUser = document.getElementById('newOwnerUser').value;

        try {
            const response = await fetch(`/lands/${id}/transfer`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ newOwnerUser })
            });

            if (response.status === 204) {
                resultDiv.innerHTML = `<div class="alert alert-success">Land title transferred successfully</div>`;
            } else {
                const error = await response.text();
                resultDiv.innerHTML = `<div class="alert alert-danger">Error: ${error}</div>`;
            }
        } catch (error) {
            resultDiv.innerHTML = `<div class="alert alert-danger">Error: ${error.message}</div>`;
        }
    });

    readLandForm.addEventListener('submit', async (event) => {
        event.preventDefault();
        const id = document.getElementById('readId').value;

        try {
            const response = await fetch(`/lands/${id}`);
            if (response.ok) {
                const landTitle = await response.json();
                resultDiv.innerHTML = `
                    <div class="alert alert-success">
                        <h3>Land Title Details</h3>
                        <p><strong>ID:</strong> ${landTitle.id}</p>
                        <p><strong>Title:</strong> ${landTitle.title}</p>
                        <p><strong>Description:</strong> ${landTitle.description}</p>
                        <p><strong>Owner User:</strong> <span class="break-word">${landTitle.ownerUser}</span></p>
                        <p><strong>Date Added:</strong> ${new Date(landTitle.dateAdded).toLocaleString()}</p>
                        <p><strong>Last Updated By:</strong> <span class="break-word">${landTitle.lastUpdatedBy}</span></p>
                        <p><strong>Last Updated:</strong> ${new Date(landTitle.lastUpdated).toLocaleString()}</p>
                    </div>
                `;
            } else {
                const error = await response.text();
                resultDiv.innerHTML = `<div class="alert alert-danger">Error: ${error}</div>`;
            }
        } catch (error) {
            resultDiv.innerHTML = `<div class="alert alert-danger">Error: ${error.message}</div>`;
        }
    });
});
