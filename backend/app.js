const express = require('express');
const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path');

const app = express();
app.use(express.json());

// Static Middleware
app.use(express.static(path.join(__dirname, 'public')));

// Function to get the contract instance
async function getContract() {
    const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const identity = await wallet.get('Admin@landregistry.com');
    const gateway = new Gateway();
    const connectionProfile = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'connection.json'), 'utf8'));
    const connectionOptions = {
        wallet,
        identity,
        discovery: {
            enabled: false,
            asLocalhost: true
        }
    };
    await gateway.connect(connectionProfile, connectionOptions);
    const network = await gateway.getNetwork('registrychannel');
    const contract = network.getContract('landmgt');
    return contract;
}

async function submitTransaction(functionName, ...args) {
    const contract = await getContract();
    const result = await contract.submitTransaction(functionName, ...args);
    return result.toString();
}

async function evaluateTransaction(functionName, ...args) {
    const contract = await getContract();
    const result = await contract.evaluateTransaction(functionName, ...args);
    return result.toString();
}

// Endpoint to create a new land title
app.post('/lands', async (req, res) => {
    try {
        const { title, description } = req.body;
        await submitTransaction('CreateLandTitle', title, description);
        res.status(204).send();
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

// Endpoint to read a land title
app.get('/lands/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const result = await evaluateTransaction('ReadLandTitle', id);
        res.status(200).send(result);
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        res.status(404).send(`Failed to evaluate transaction: ${error}`);
    }
});

// Endpoint to update a land title
app.put('/lands/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const { title, description } = req.body;
        await submitTransaction('UpdateLandTitle', id, title, description);
        res.status(204).send();
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

// Endpoint to transfer ownership of a land title
app.put('/lands/:id/transfer', async (req, res) => {
    try {
        const { id } = req.params;
        const { newOwnerUser } = req.body;
        await submitTransaction('TransferLandTitle', id, newOwnerUser);
        res.status(204).send();
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

// Endpoint to get the total number of land titles
app.get('/lands/total', async (req, res) => {
    try {
        const result = await evaluateTransaction('GetTotalLandTitles');
        res.status(200).send(result);
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        res.status(500).send(`Failed to evaluate transaction: ${error}`);
    }
});



module.exports = app;
