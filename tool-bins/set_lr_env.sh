#!/bin/bash
#Sets the context for native peer commands

# function usage {
#     echo "Usage:             . ./set_lr_env.sh  ORG_NAME"
#     echo "           Sets the organization context for native peer execution"
# }

# if [ "$1" == "" ]; then
#     usage
#     exit
# fi
# export ORG_CONTEXT=$1
# MSP_ID="$(tr '[:lower:]' '[:upper:]' <<< ${ORG_CONTEXT:0:1})${ORG_CONTEXT:1}"
# export ORG_NAME=$MSP_ID

# Added this Oct 22
# export CORE_PEER_LOCALMSPID=LandRegistryMSP

# # Logging specifications
# export FABRIC_LOGGING_SPEC=INFO

# # Location of the core.yaml
# export FABRIC_CFG_PATH=/workspaces/LTRegistry/config/lr

# # Address of the peer
# export CORE_PEER_ADDRESS=landregistry.landtitleregistry.com:7051

# # Local MSP for the admin - Commands need to be executed as org admin
# export CORE_PEER_MSPCONFIGPATH=/workspaces/LTRegistry/config/crypto-config/peerOrganizations/landregistry.com/users/Admin@landregistry.com/msp

# # Address of the orderer
# export ORDERER_ADDRESS=orderer.landtitleregistry.com:7050
# export CORE_PEER_TLS_ENABLED=false

# !/bin/bash
export ORG_CONTEXT=ltregistry
export ORG_NAME=LandRegistry
export CORE_PEER_LOCALMSPID=LandRegistryMSP

# Logging specifications
export FABRIC_LOGGING_SPEC=INFO

# Location of the core.yaml
export FABRIC_CFG_PATH=/workspaces/LTRegistry/config/lr

# Address of the peer
export CORE_PEER_ADDRESS=landregistry.landtitleregistry.com:7051

# Local MSP for the admin - Commands need to be executed as org admin
export CORE_PEER_MSPCONFIGPATH=/workspaces/LTRegistry/config/crypto-config/peerOrganizations/landregistry.com/users/Admin@landregistry.com/msp

# Address of the orderer
export ORDERER_ADDRESS=orderer.landtitleregistry.com:7050
export CORE_PEER_TLS_ENABLED=false

#### Chaincode related properties
export CC_NAME="landmgt"
export CC_PATH="./chaincodes/landmgt/"
export CC_CHANNEL_ID="registrychannel"
export CC_LANGUAGE="golang"

# Properties of Chaincode
export INTERNAL_DEV_VERSION="1.0"
export CC_VERSION="1.0"
export CC2_PACKAGE_FOLDER="./chaincodes/packages/"
export CC2_SEQUENCE=1
export CC2_INIT_REQUIRED="--init-required"

# Create the package with this name
export CC_PACKAGE_FILE="$CC2_PACKAGE_FOLDER$CC_NAME.$CC_VERSION-$INTERNAL_DEV_VERSION.tar.gz"

# Extracts the package ID for the installed chaincode
export CC_LABEL="$CC_NAME.$CC_VERSION-$INTERNAL_DEV_VERSION"
peer channel list
