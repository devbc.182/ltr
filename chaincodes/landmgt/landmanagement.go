package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

// LandTitle represents a land title added by an organization.
type LandTitle struct {
	ID            string    `json:"id"`
	Title         string    `json:"title"`
	Description   string    `json:"description"`
	OwnerUser     string    `json:"ownerUser"`
	DateAdded     time.Time `json:"dateAdded"`
	LastUpdatedBy string    `json:"lastUpdatedBy"`
	LastUpdated   time.Time `json:"lastUpdated"`
}

// LandTitleContract provides functions for managing land titles.
type LandTitleContract struct {
	contractapi.Contract
}

// AutoIncrementIDKey is the key used to store the auto-incrementing ID counter in the ledger.
const AutoIncrementIDKey = "landTitleIDCounter"

// CreateLandTitle adds a new land title to the ledger with an auto-incrementing ID.
func (c *LandTitleContract) CreateLandTitle(ctx contractapi.TransactionContextInterface, title string, description string) error {
	// Get the current counter value
	counter, err := c.getAndIncrementCounter(ctx)
	if err != nil {
		return err
	}

	// Create the land title ID using the counter value
	id := strconv.Itoa(counter)

	clientIdentity := ctx.GetClientIdentity()
	creator, err := clientIdentity.GetID()
	if err != nil {
		return fmt.Errorf("error getting creator ID: %v", err)
	}

	landTitle := LandTitle{
		ID:            id,
		Title:         title,
		Description:   description,
		OwnerUser:     creator,
		DateAdded:     time.Now(),
		LastUpdatedBy: creator,
		LastUpdated:   time.Now(),
	}

	landTitleJSON, err := json.Marshal(landTitle)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(id, landTitleJSON)
	if err != nil {
		return fmt.Errorf("failed to put land title to world state: %v", err)
	}

	eventPayload := fmt.Sprintf("Created land title: %s", id)
	err = ctx.GetStub().SetEvent("CreateLandTitle", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register: %v", err)
	}

	return nil
}

// getAndIncrementCounter retrieves the current counter value and increments it.
func (c *LandTitleContract) getAndIncrementCounter(ctx contractapi.TransactionContextInterface) (int, error) {
	counterAsBytes, err := ctx.GetStub().GetState(AutoIncrementIDKey)
	if err != nil {
		return 0, fmt.Errorf("failed to read counter from world state: %v", err)
	}

	var counter int
	if counterAsBytes == nil {
		counter = 0
	} else {
		counter, err = strconv.Atoi(string(counterAsBytes))
		if err != nil {
			return 0, fmt.Errorf("failed to convert counter to integer: %v", err)
		}
	}

	counter++
	counterAsBytes = []byte(strconv.Itoa(counter))

	err = ctx.GetStub().PutState(AutoIncrementIDKey, counterAsBytes)
	if err != nil {
		return 0, fmt.Errorf("failed to update counter in world state: %v", err)
	}

	return counter, nil
}

// GetTotalLandTitles returns the total number of land titles.
func (c *LandTitleContract) GetTotalLandTitles(ctx contractapi.TransactionContextInterface) (int, error) {
	counterAsBytes, err := ctx.GetStub().GetState(AutoIncrementIDKey)
	if err != nil {
		return 0, fmt.Errorf("failed to read counter from world state: %v", err)
	}

	if counterAsBytes == nil {
		return 0, nil // No land titles added yet
	}

	counter, err := strconv.Atoi(string(counterAsBytes))
	if err != nil {
		return 0, fmt.Errorf("failed to convert counter to integer: %v", err)
	}

	return counter, nil
}

// ReadLandTitle reads an existing land title from the ledger.
func (c *LandTitleContract) ReadLandTitle(ctx contractapi.TransactionContextInterface, id string) (*LandTitle, error) {
	landTitleJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if landTitleJSON == nil {
		return nil, fmt.Errorf("the land title %s does not exist", id)
	}

	var landTitle LandTitle
	err = json.Unmarshal(landTitleJSON, &landTitle)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal land title JSON: %v", err)
	}

	eventPayload := fmt.Sprintf("Read land title: %s", id)
	err = ctx.GetStub().SetEvent("ReadLandTitle", []byte(eventPayload))
	if err != nil {
		return nil, fmt.Errorf("event failed to register: %v", err)
	}

	return &landTitle, nil
}

// UpdateLandTitle updates an existing land title in the ledger.
func (c *LandTitleContract) UpdateLandTitle(ctx contractapi.TransactionContextInterface, id string, title string, description string) error {
	landTitle, err := c.ReadLandTitle(ctx, id)
	if err != nil {
		return err
	}

	clientIdentity := ctx.GetClientIdentity()
	creator, err := clientIdentity.GetID()
	if err != nil {
		return fmt.Errorf("error getting creator ID: %v", err)
	}

	landTitle.Title = title
	landTitle.Description = description
	landTitle.LastUpdatedBy = creator
	landTitle.LastUpdated = time.Now()

	landTitleJSON, err := json.Marshal(landTitle)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(id, landTitleJSON)
	if err != nil {
		return fmt.Errorf("failed to put land title to world state: %v", err)
	}

	eventPayload := fmt.Sprintf("Updated land title: %s", id)
	err = ctx.GetStub().SetEvent("UpdateLandTitle", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register: %v", err)
	}

	return nil
}

// TransferLandTitle transfers the ownership of a land title to a new owner.
func (c *LandTitleContract) TransferLandTitle(ctx contractapi.TransactionContextInterface, id string, newOwnerUser string) error {
	landTitle, err := c.ReadLandTitle(ctx, id)
	if err != nil {
		return err
	}

	clientIdentity := ctx.GetClientIdentity()
	updater, err := clientIdentity.GetID()
	if err != nil {
		return fmt.Errorf("error getting updater ID: %v", err)
	}

	landTitle.OwnerUser = newOwnerUser
	landTitle.LastUpdatedBy = updater
	landTitle.LastUpdated = time.Now()

	landTitleJSON, err := json.Marshal(landTitle)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(id, landTitleJSON)
	if err != nil {
		return fmt.Errorf("failed to put land title to world state: %v", err)
	}

	eventPayload := fmt.Sprintf("Transferred land title: %s to %s", id, newOwnerUser)
	err = ctx.GetStub().SetEvent("TransferLandTitle", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register: %v", err)
	}

	return nil
}

func main() {
	chaincode, err := contractapi.NewChaincode(new(LandTitleContract))
	if err != nil {
		fmt.Printf("Error creating land title contract: %s", err.Error())
		return
	}
	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting land title contract: %s", err.Error())
	}
}
