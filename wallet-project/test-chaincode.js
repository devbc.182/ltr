// const { Gateway, Wallets } = require('fabric-network');
// const fs = require('fs');
// const path = require('path');
// function prettyJSONString(inputString) {
//     return JSON.stringify(JSON.parse(inputString), null, 2);
// }
// async function main() {
//     // Load the network configuration
//     const ccpPath = path.resolve(__dirname, '.', 'connection.json');
//     const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));
//     // Create a new file system based wallet for managing identities.
//     const walletPath = path.join(process.cwd(), 'wallet');
//     const wallet = await Wallets.newFileSystemWallet(walletPath);
//     // Check to see if we've already enrolled the user.
//     const identity = await wallet.get('Admin@landregistry.com');
//     if (!identity) {
//         console.log(`An identity for the user "${identity}" does not exist in the wallet`);
//         return;
//     }
//     console.log("identity",identity)
//     // Create a new gateway instance for interacting with the fabric network.
//     const gateway = new Gateway();
//     console.log("wallet",wallet)
//     try {
//         // Connect to the gateway using the identity from wallet and the connection profile.
//         await gateway.connect(ccp, {
//             wallet, identity: identity, discovery: {
//                 enabled: false, asLocalhost: true
//             }
//         });
//         // Now connected to the gateway.
//         console.log('Connected to the gateway.');
//         // ... you can now use the gateway ...
//         // For example, get a contract and submit a transaction
//         const network = await gateway.getNetwork('registrychannel');
//         const contract = network.getContract('landmgt');
//         // console.log("network",network)
//         // console.log("contract",contract)

//         // //CREATE
//         // // console.log('\n--> Submit Transaction: Create, function creates');
//         await contract.submitTransaction('CreateLandTitle', 'Dechen Namegay', 'how');
//         // // console.log('*** Result: committed');
//         // // // //READ
//         // console.log('\n--> Evaluate Transaction: ReadStudentr');
//         // let result = await contract.evaluateTransaction('ReadLandTitle', '1');
//         // console.log(`*** Result: ${prettyJSONString(result.toString())}`);
//     } finally {
//         // Disconnect from the gateway when you're done.
//         gateway.disconnect();
//     }
// }
// main().catch(console.error);




const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path');

function prettyJSONString(inputString) {
    return JSON.stringify(JSON.parse(inputString), null, 2);
}

async function main() {
    const ccpPath = path.resolve(__dirname, 'connection.json');
    const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));

    const walletPath = path.join(process.cwd(), 'wallet');
    console.log("walletPath", walletPath)
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    console.log("wallet", wallet)

    const identity = await wallet.get('Admin@landregistry.com');
    if (!identity) {
        console.log(`An identity for the user "Admin@bhupurchasingweapon.com" does not exist in the wallet`);
        return;
    }

    const gateway = new Gateway();
    try {
        await gateway.connect(ccp, {
            wallet, identity: identity, discovery: { enabled: false, asLocalhost: true }
        });

        console.log('Connected to the gateway.');

        const network = await gateway.getNetwork('registrychannel');
        const contract = network.getContract('landmgt');

        console.log('\n--> Submit Transaction: Create, function creates');
        await contract.submitTransaction('CreateLandTitle', 'paro', "land in paro");
        console.log('*** Result: committed');


        console.log('\n--> Submit Transaction: ReadWeapon');
        let result = await contract.evaluateTransaction('ReadLandTitle', '2');
        console.log(`*** Result: ${prettyJSONString(result.toString())}`);

    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
    } finally {
        gateway.disconnect();
    }
}

main().catch(console.error);